<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes = array(
        'xv_homepage' => array (  0 =>   array (    0 => 'page',  ),  1 =>   array (    '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::indexAction',    'page' => 1,  ),  2 =>   array (    'page' => '\\d*',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d*',      3 => 'page',    ),    1 =>     array (      0 => 'text',      1 => '/xv',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'xv_voir' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::voirAction',  ),  2 =>   array (    'page' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/xv/article',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'xv_ajouter' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::ajouterAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/xv/ajouter',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'xv_modifier' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::modifierAction',  ),  2 =>   array (    'page' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/xv/modifier',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'xv_supprimer' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::supprimerAction',  ),  2 =>   array (    'page' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/xv/supprimer',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
