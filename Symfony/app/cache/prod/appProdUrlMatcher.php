<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/xv')) {
            // xv_homepage
            if (preg_match('#^/xv(?:/(?P<page>\\d*))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'xv_homepage')), array (  '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::indexAction',  'page' => 1,));
            }

            if (0 === strpos($pathinfo, '/xv/a')) {
                // xv_voir
                if (0 === strpos($pathinfo, '/xv/article') && preg_match('#^/xv/article/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'xv_voir')), array (  '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::voirAction',));
                }

                // xv_ajouter
                if ($pathinfo === '/xv/ajouter') {
                    return array (  '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::ajouterAction',  '_route' => 'xv_ajouter',);
                }

            }

            // xv_modifier
            if (0 === strpos($pathinfo, '/xv/modifier') && preg_match('#^/xv/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'xv_modifier')), array (  '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::modifierAction',));
            }

            // xv_supprimer
            if (0 === strpos($pathinfo, '/xv/supprimer') && preg_match('#^/xv/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'xv_supprimer')), array (  '_controller' => 'XvDuHome\\XvBundle\\Controller\\XvController::supprimerAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
