<?php

/* XvBundle:Xv:index.html.twig */
class __TwigTemplate_3a0b5dbff220110b6ffe340b1587c36f7455a74f363291872ff2dbe025ec3ced extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("XvBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'xv_body' => array($this, 'block_xv_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "XvBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "  Accueil - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_xv_body($context, array $blocks = array())
    {
        // line 8
        echo "
  <h2>Liste des articles</h2>

  <ul>
    ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 13
            echo "      <li>
        <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("xv_voir", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "titre"), "html", null, true);
            echo "</a>
        par ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "auteur"), "html", null, true);
            echo ",
        le ";
            // line 16
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "date"), "d/m/Y"), "html", null, true);
            echo "
      </li>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 19
            echo "      <li>Pas (encore !) d'articles</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "  </ul>
  
  <div>
    <ul  class=\"pagination\">
      ";
        // line 26
        echo "\t\t";
        if (((isset($context["page"]) ? $context["page"] : null) > 1)) {
            echo " 
\t\t\t<li><a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("xv_homepage", array("page" => ((isset($context["page"]) ? $context["page"] : null) - 1))), "html", null, true);
            echo "\">&laquo;</a></li> 
\t\t";
        }
        // line 29
        echo "      ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["nombrePage"]) ? $context["nombrePage"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 30
            echo "        <li";
            if (((isset($context["p"]) ? $context["p"] : null) == (isset($context["page"]) ? $context["page"] : null))) {
                echo " class=\"active\"";
            }
            echo ">
          <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("xv_homepage", array("page" => (isset($context["p"]) ? $context["p"] : null))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["p"]) ? $context["p"] : null), "html", null, true);
            echo "</a>
        </li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "\t\t<li><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("xv_homepage", array("page" => ((isset($context["page"]) ? $context["page"] : null) + 1))), "html", null, true);
        echo "\">&raquo;</a></li>
    </ul>
  </div>

";
    }

    public function getTemplateName()
    {
        return "XvBundle:Xv:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 34,  109 => 31,  102 => 30,  97 => 29,  92 => 27,  87 => 26,  81 => 21,  74 => 19,  66 => 16,  62 => 15,  56 => 14,  53 => 13,  48 => 12,  42 => 8,  39 => 7,  32 => 4,  29 => 3,);
    }
}
