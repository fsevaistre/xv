<?php

/* ::layout.html.twig */
class __TwigTemplate_f7d3ce329db0494dc3481f952a6d9df9c018831e3571024a138e26be7ce4652b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t
\t<meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <link rel=\"shortcut icon\" href=\"../../docs-assets/ico/favicon.png\">
\t<title>";
        // line 13
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t
\t";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "\t
\t<link href=\"css/starter-template.css\" rel=\"stylesheet\">
\t<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>

<body>

\t<nav class=\"navbar navbar-inverse navbar-fixed-top\">
\t  <ul class=\"nav navbar-nav navbar-static-top\">
\t\t<li style=\"margin: 5px 30px 5px 30px;\"> <img src=\"img/logo.png\" height=\"40px\"> </li>
\t\t<li class=\"active\"> <a href=\"#\">Accueil</a> </li>
\t\t<li class=\"dropdown\">
\t\t\t<a data-toggle=\"dropdown\" href=\"#\">Informations<b class=\"caret\"></b></a> 
\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t<li><a href=\"#\">L'équipe</a></li>
\t\t\t\t<li><a href=\"#\">Les joueurs</a></li>
\t\t\t\t<li><a href=\"#\">Contact</a></li>
\t\t\t</ul>
\t\t</li>
\t\t<li class=\"dropdown\">
\t\t\t<a data-toggle=\"dropdown\" href=\"#\">Les matchs<b class=\"caret\"></b></a> 
\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t<li><a href=\"#\">Calendrier / résultats</a></li>
\t\t\t\t<li><a href=\"#\">Résumés</a></li>
\t\t\t</ul>
\t\t</li>
\t\t<li> <a href=\"#\">Partenaires</a> </li>
\t\t<li> <a href=\"#\">Photos</a> </li>
\t\t<li class=\"hidden-lg\"> <a href=\"#\">Connexion</a> </li>
\t  </ul>
\t  
\t  <form class=\"navbar-form pull-right visible-lg\">
\t\t\t<input id=\"identifiant\" name=\"identifiant\" type=\"text\" placeholder=\"Identifiant\" class=\"input-sm form-control\" required=\"\">
\t\t\t<input id=\"mdp\" name=\"mdp\" type=\"password\" placeholder=\"Mot de passe\" class=\"input-sm form-control\" required=\"\">
\t\t\t<button id=\"connexion\" name=\"connexion\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-user\"></span> Connexion</button>
\t\t</form>

\t</nav>
\t
\t<div class=\"container\">
      <div class=\"row\">
        <div id=\"content\" class=\"col-lg-9\">
          ";
        // line 66
        $this->displayBlock('body', $context, $blocks);
        // line 68
        echo "        </div>
\t\t<div id=\"menu\" class=\"col-lg-3\">
          <h3>Le blog</h3>
          <ul class=\"nav nav-pills nav-stacked\">
            <li><a href=\"";
        // line 72
        echo $this->env->getExtension('routing')->getPath("xv_homepage");
        echo "\">Accueil du blog</a></li>
            <li><a href=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("xv_ajouter");
        echo "\">Ajouter un article</a></li>
          </ul>
                    
          ";
        // line 76
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("XvBundle:Xv:menu", array("nombre" => 3)));
        echo "
        </div>
      </div>
    </div>

\t  <div class=\"navbar navbar-default navbar-fixed-bottom\">
    <div class=\"container\">
      <p class=\"navbar-text pull-left\">© 2014 - XV du Home
      </p>
      
      <a href=\"mentions-legales.php\" class=\"navbar-btn btn btn-primary btn-sm pull-right\">
      <span class=\"glyphicon glyphicon-star\"></span>  Mentions Légales</a>
    </div>   
  </div>
  
    ";
        // line 91
        $this->displayBlock('javascripts', $context, $blocks);
        // line 95
        echo "  </body>
</html>";
    }

    // line 13
    public function block_title($context, array $blocks = array())
    {
        echo "XV du Home ";
    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 16
        echo "      <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" type=\"text/css\" />
      <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/starter-template.css"), "html", null, true);
        echo "\" type=\"text/css\" />
    ";
    }

    // line 66
    public function block_body($context, array $blocks = array())
    {
        // line 67
        echo "          ";
    }

    // line 91
    public function block_javascripts($context, array $blocks = array())
    {
        // line 92
        echo "\t\t<script src=\"https://code.jquery.com/jquery.js\"></script>
\t\t<script type=\"text/javascript\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t";
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 93,  166 => 92,  163 => 91,  159 => 67,  156 => 66,  150 => 17,  145 => 16,  142 => 15,  136 => 13,  131 => 95,  129 => 91,  111 => 76,  105 => 73,  101 => 72,  95 => 68,  93 => 66,  44 => 19,  37 => 13,  23 => 1,  63 => 16,  60 => 15,  55 => 17,  52 => 15,  46 => 10,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  120 => 34,  109 => 31,  102 => 30,  97 => 29,  92 => 27,  87 => 26,  81 => 21,  74 => 19,  66 => 16,  62 => 15,  56 => 14,  53 => 13,  48 => 12,  42 => 15,  39 => 7,  32 => 4,  29 => 3,);
    }
}
