<?php

/* XvBundle:Xv:menu.html.twig */
class __TwigTemplate_c9b1da0d5b665ee0e3b3c5a372099e05fda938aa1f1c31befd9ac0d6a4466ec6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Les derniers articles</h3>

<ul class=\"nav nav-pills nav-stacked\">
  ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["liste_articles"]) ? $context["liste_articles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("xv_voir", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "titre"), "html", null, true);
            echo "</a></li>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "XvBundle:Xv:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  24 => 4,  19 => 1,  169 => 93,  166 => 92,  163 => 91,  159 => 67,  156 => 66,  150 => 17,  145 => 16,  142 => 15,  136 => 13,  131 => 95,  129 => 91,  111 => 76,  105 => 73,  101 => 72,  95 => 68,  93 => 66,  44 => 19,  37 => 13,  23 => 1,  63 => 16,  60 => 15,  55 => 17,  52 => 15,  46 => 10,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  120 => 34,  109 => 31,  102 => 30,  97 => 29,  92 => 27,  87 => 26,  81 => 21,  74 => 19,  66 => 16,  62 => 15,  56 => 14,  53 => 13,  48 => 12,  42 => 15,  39 => 7,  32 => 4,  29 => 3,);
    }
}
