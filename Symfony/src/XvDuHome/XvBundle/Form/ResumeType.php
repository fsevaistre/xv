<?php

namespace XvDuHome\XvBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResumeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date','date')
            ->add('titre','text')
            ->add('auteur','text')
            ->add('contenu','textarea')
            ->add('publication','checkbox',array('required'=>false))
			->add('image',new ImageType())
			// ->add('categories', 'collection', array('type'         => new CategorieType(),
                                              // 'allow_add'    => true,
                                              // 'allow_delete' => true))
			->add('categories', 'entity', array(
											  'class'    => 'XvBundle:Categorie',
											  'property' => 'nom',
											  'multiple' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'XvDuHome\XvBundle\Entity\Resume'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'xvduhome_xvbundle_resume';
    }
}
