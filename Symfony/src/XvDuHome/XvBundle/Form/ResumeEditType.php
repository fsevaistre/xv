<?php

namespace XvDuHome\XvBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResumeEditType extends ResumeEditType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);
        $builder
            ->remove('date')
        ;
    }
	
	public function getName(){
		return 'sdz_blogbundle_articleedittype';
	}
}
