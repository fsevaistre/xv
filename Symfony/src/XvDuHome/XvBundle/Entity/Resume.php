<?php

namespace XvDuHome\XvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resume
 *
 * @ORM\Table("xv_resume")
 * @ORM\Entity(repositoryClass="XvDuHome\XvBundle\Entity\ResumeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Resume
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=255)
     */
    private $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;
	
	/**
	* @ORM\Column(name="publication", type="boolean")
	*/
	private $publication;
  
	/**
	* @ORM\OneToOne(targetEntity="XvDuHome\XvBundle\Entity\Image", cascade={"persist"})
	*/
	private $image;
	
	/**
	* @ORM\ManyToMany(targetEntity="XvDuHome\XvBundle\Entity\Categorie", cascade={"persist"})
	*/
	private $categories;
	
	/**
	* @ORM\OneToMany(targetEntity="XvDuHome\XvBundle\Entity\Commentaire", mappedBy="article",cascade={"persist"})
	*/
	private $commentaires;
	
	/**
	*@ORM\Column(name="date_edition",type="datetime")
	*/
	private $dateEdition;
	
	/**
	*@ORM\Column(name="nb_commentaires",type="integer",nullable=true)
	*/
	private $nbCommentaires;
  
	public function __construct(){
		$this->date = new \Datetime(); // Par défaut, la date de l'article est la date d'aujourd'hui
		$this->publication = true;
		$this->dateEdition = new \Datetime();
		$this->categories   = new \Doctrine\Common\Collections\ArrayCollection();
		$this->commentaires = new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Resume
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Resume
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Resume
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Resume
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set publication
     *
     * @param boolean $publication
     * @return Resume
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return boolean 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set image
     *
     * @param \Xv\XvBundle\Entity\Image $image
     * @return Resume
     */
    public function setImage(\XvDuHome\XvBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Xv\XvBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add categories
     *
     * @param \XvDuHome\XvBundle\Entity\Categorie $categories
     * @return Resume
     */
    public function addCategorie(\XvDuHome\XvBundle\Entity\Categorie $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \XvDuHome\XvBundle\Entity\Categorie $categories
     */
    public function removeCategorie(\XvDuHome\XvBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add categories
     *
     * @param \XvDuHome\XvBundle\Entity\Categorie $categories
     * @return Resume
     */
    public function addCategory(\XvDuHome\XvBundle\Entity\Categorie $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \XvDuHome\XvBundle\Entity\Categorie $categories
     */
    public function removeCategory(\XvDuHome\XvBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Add commentaires
     *
     * @param \XvDuHome\XvBundle\Entity\Commentaire $commentaires
     * @return Resume
     */
    public function addCommentaire(\XvDuHome\XvBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires[] = $commentaires;
		$commentaires->setArticle($this);
        return $this;
    }

    /**
     * Remove commentaires
     *
     * @param \XvDuHome\XvBundle\Entity\Commentaire $commentaires
     */
    public function removeCommentaire(\XvDuHome\XvBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires->removeElement($commentaires);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
	
	/**
	* @ORM\PreUpdate
	*/
	public function updateDate(){
		$this -> setDateEdition(new \Datetime());
	}

    /**
     * Set dateEdition
     *
     * @param \DateTime $dateEdition
     * @return Resume
     */
    public function setDateEdition($dateEdition)
    {
        $this->dateEdition = $dateEdition;

        return $this;
    }

    /**
     * Get dateEdition
     *
     * @return \DateTime 
     */
    public function getDateEdition()
    {
        return $this->dateEdition;
    }

    /**
     * Set nbCommentaires
     *
     * @param integer $nbCommentaires
     * @return Resume
     */
    public function setNbCommentaires($nbCommentaires)
    {
        $this->nbCommentaires = $nbCommentaires;

        return $this;
    }

    /**
     * Get nbCommentaires
     *
     * @return integer 
     */
    public function getNbCommentaires()
    {
        return $this->nbCommentaires;
    }
}
