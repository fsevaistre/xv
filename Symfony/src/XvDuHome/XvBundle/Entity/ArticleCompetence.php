<?php

namespace XvDuHome\XvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleCompetence
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XvDuHome\XvBundle\Entity\ArticleCompetenceRepository")
 */
class ArticleCompetence
{
    /**
     *@ORM\Id
     * @ORM\ManyToOne(targetEntity="XvDuHome\XvBundle\Entity\Resume")
     */
    private $article;

    /**
     *@ORM\Id
     * @ORM\ManyToOne(targetEntity="XvDuHome\XvBundle\Entity\Resume")
     */
    private $competence;

    /**
     * @ORM\Column()
     */
    private $niveau;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param string $article
     * @return ArticleCompetence
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return string 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set competence
     *
     * @param string $competence
     * @return ArticleCompetence
     */
    public function setCompetence($competence)
    {
        $this->competence = $competence;

        return $this;
    }

    /**
     * Get competence
     *
     * @return string 
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * Set niveau
     *
     * @param string $niveau
     * @return ArticleCompetence
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return string 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }
}
