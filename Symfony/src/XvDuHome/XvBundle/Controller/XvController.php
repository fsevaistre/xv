<?php

namespace XvDuHome\XvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use XvDuHome\XvBundle\Entity\Resume;
use XvDuHome\XvBundle\Entity\Image;
use XvDuHome\XvBundle\Entity\Commentaire;
use XvDuHome\XvBundle\Entity\Categorie;
use XvDuHome\XvBundle\Form\ResumeType;


class XvController extends Controller{
	public function indexAction($page){
		$repository = $this	->getDoctrine()
											->getManager()
											->getRepository("XvBundle:Resume");
		$articles = $repository -> getResumes(3,$page);
		return $this->render('XvBundle:Xv:index.html.twig',array(
																			'articles'=>$articles,
																			'page'       => $page,
																			'nombrePage' => ceil(count($articles)/3)
																			));
	}


	public function voirAction(Resume $resume){
		 $em = $this->getDoctrine()
               ->getManager();
		$liste_commentaires = $em->getRepository('XvBundle:Commentaire')
															->findAll();
		$resume -> getCategories();
		return $this->render('XvBundle:Xv:voir.html.twig', array(
			'article'		   => $resume,
			'liste_commentaires' => $liste_commentaires
		));
	}

	public function ajouterAction(){
		$article = new Resume();
		$form = $this -> createForm(new ResumeType, $article);
		
		$request = $this->get('request');
		if ($request->getMethod() == 'POST') {
			$form->bind($request);
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($article);
				$em->flush();
				return $this->redirect($this->generateUrl('xv_voir', array('id' => $article->getId())));
			}
		}
		return $this->render('XvBundle:Xv:ajouter.html.twig', array('form' => $form->createView(),));
	}
	
	public function modifierAction(Resume $resume){
		$resume -> getCategories();
		$form = $this -> createForm(new ResumeType, $resume);
												
		$request = $this->get('request');
		if ($request->getMethod() == 'POST') {
			$form->bind($request);
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($resume);
				$em->flush();
				return $this->redirect($this->generateUrl('xv_modifier', array('id' => $resume->getId())));
			}
		}
		return $this->render('XvBundle:Xv:modifier.html.twig', array('form' => $form->createView(),'resume' => $resume));
	}

	public function supprimerAction(Resume $resume){
		
		// Ici, on récupérera l'article correspondant à $id
		// Ici, on gérera la suppression de l'article en question
		return $this->render('XvBundle:Xv:supprimer.html.twig');
	}
	
	public function menuAction($nombre){
	$repository = $this->getDoctrine()
                   ->getManager()
                   ->getRepository('XvBundle:Resume');

	$listeArticles = $repository->findBy(array(),array("date"=>"desc"),$nombre,0);
	return $this->render('XvBundle:Xv:menu.html.twig', array('liste_articles' => $listeArticles));
	}
}