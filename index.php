<?php require_once("outils.php");
	entete("XV du Home");
?>

    <div class="container">
		
		<header class="row">
			<div class="col-md-12">Entête</div>
		</header>
		
		<div class="row">
		
			<div class="col-sm-2">
				<div class="row">
					<div class="col-md-12">Aside 1</div>
					<div class="col-md-12">Aside 2</div>
				</div>
			</div>
			
			<div class="col-sm-10 col-md-8">Section</div>
			<div class="clearfix visible-sm"></div>
			<div class="col-md-2 hidden-xs">
				<div class="row">
					<div class="col-md-12">Aside</div>
					<div class="col-md-12">Aside</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
  <form class="form-inline well">
    <div class="form-group">
      <label class="sr-only" for="text">Saisie</label>
      <input id="text" type="text" class="form-control" placeholder="Texte ici">
    </div>
    <button type="submit" class="btn btn-primary pull-right">Envoyer</button>
    <div class="alert alert-block alert-danger" style="display:none">
      <h4>Erreur !</h4>
      Vous devez entrer au moins 4 caractères ! 
    </div>
  </form>
</div>

		
		<footer class="row">
			<div class="col-md-12">Pied de page</div>
		</footer>
	</div>
	
	<?php pied(); ?>